"""
Redshift connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: Apr 26, 2017

RedshiftUtils.
"""

import logging_helper as Log
import psycopg2 as ppg2


class RedshiftConnector():
    """RedshiftUtils class takes in the arguments database, databasehost, port, user, password, level at the
    time of initiation.

    :param database: Provide the database name to connect to.
    :param databasehost: Provide the database host name.
    :param port: Provide the port.
    :param user: Provide the user name to use when initiating connection.
    :param password: Provide the password to initiate connection.
    :param level: Provide the log level ('DEBUG', 'INFO' etc).
    """
    conn = None

    def __init__(self, database, databasehost, port, user, password, level):
        self.logs = Log.log(level)
        self.logs.info('SQLConnector initialized')
        self.database = database
        self.databasehost = databasehost
        self.user = user
        self.port = port
        self.password = password
        self._connect()

    def _connect(self):
        """This method makes the connection to RedShift database.
        """
        try:

            self.conn = ppg2.connect(dbname=self.database, host=self.databasehost,
                               port=self.port, user=self.user, password=self.password)

        except Exception as e:
            self.logs.error(e)
            raise e

    def query(self, sql, params=()):
        """This method performs the query on the database and returns the rows from the response.

        :param sql: Takes the constructed sql statement from the methods which uses it (example 'read_all_fields').
        :param params: Provide the extra aguments if there are any.
        """
        sql_query = ''
        for item in sql:
            sql_query += item + ' '
        try:
            self.logs.info(sql_query)
            cursor = self.conn.cursor()
            cursor.execute(sql_query, params)
            rows = cursor.fetchall()
            cursor.close()
        except (AttributeError, ppg2.OperationalError) as e:
            self.logs.error('No connection is open so new connection would be opened')
            self._connect()
            cursor = self.conn.cursor()
            cursor.execute(sql_query, params)
            rows = cursor.fetchall()
            cursor.close()
        return rows

    def commit(self):
        """This method is used to commit the transaction.
        """
        try:
            if self.conn:
                self.conn.commit()
                self.logs.info('Commited transaction')
            else:
                self.logs.info('No Database Connection to commit changes')
        except Exception as e:
            self.logs.Error(e)
            raise e

    def close_db_connection(self):
        """This method is used to close the database connection which was established.
        """
        try:
            if self.conn:
                self.conn.close()
                self.logs.info('Closed Database Connection')
            else:
                self.logs.info('No Database Connection to Close.')
        except (AttributeError, ppg2.OperationalError) as e:
            self.logs.Error(e)
            raise e

    def start_transaction(self):
        """This method is used to begin a transaction.
        """
        return self.conn.begin()

    def rollback(self):
        """This method is used to rollback the changes any changes.
        """
        self.conn.rollback()
        self.conn.close()

    def read_all_fields(self, table, **kwargs):
        """ Generates SQL for a SELECT statement matching the kwargs passed.
            read_all_fields("database.tablename")
            read_all_fields("database.tablename", **{"column1": "XYZ"})
            read_all_fields("database.tablename", **{"column1": "XYZ", "column2": "ABC"})

        :param table: Provide the table name to perform the query on.
        :param **kwargs: Provide the arguments in the form of the above example.
        """
        sql = list()
        sql.append("SELECT * FROM %s " % table)
        if kwargs:
            sql.append("WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return self.query(sql)

    def read_specific_fields(self, fields, table, **kwargs):
        """ Generates SQL for a SELECT statement matching the kwargs passed.
            read_specific_fields("column1, column2", "database.tablename")
            read_specific_fields("column1, column2", "database.tablename",  **{"column1": "XYZ"})
            read_specific_fields("column1, column2", "database.tablename",  **{"column1": "XYZ", "column2": "ABC"})

        :param fields: Provide the fields (column names) to read from the table.
        :param table: Provide the table name to perform the query on.
        :param **kwargs: Provide the arguments in the form of the above example.
        """
        sql = list()
        sql.append("SELECT %s FROM %s" % (fields, table))
        if kwargs:
            sql.append(" WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return self.query(sql)

    def read_all_fields_where_less_greater_than(self, table, wherefields):
        """ Generates SQL for a SELECT statement matching the kwargs passed.
            read_specific_fields_specialcase( "database.tablename1 a, database.tablename2 b", "a.cloumn1 = 'XYZ' AND b.column < "{}"'.format(abc))
            "SELECT * FROM database.tablename1 a, database.tablename2 b WHERE a.cloumn1 = 'XYZ' AND b.column < 'abc'"

        :param table: Provide the table name to perform the query on.
        :param wherefields: Provide the comparator fields like above example.
        """
        sql = list()
        sql.append("SELECT * FROM {:}".format(table))
        sql.append(" WHERE {:}".format(wherefields))
        sql.append(";")
        return self.query(sql)

    def read_specific_fields_where_less_greater_than(self, fields, table, wherefields):
        """ Generates SQL for a SELECT statement matching the kwargs passed.
            read_specific_fields_specialcase( 'a.cloumn1, b.cloumn1", "database.tablename1 a, database.tablename2 b", "a.cloumn1 = 'XYZ' AND b.column < "{}"'.format(abc))
            "SELECT a.cloumn1, b.cloumn1 FROM database.tablename1 a, database.tablename2 b WHERE a.cloumn1 = 'XYZ' AND b.column < 'abc'"

        :param fields: Provide the fields (column names) to read from the table.
        :param table: Provide the table name to perform the query on.
        :param wherefields: Provide the comparator fields like above example.
        """
        sql = list()
        sql.append("SELECT {:} FROM {:}".format(fields, table))
        sql.append(" WHERE {:}".format(wherefields))
        sql.append(";")
        return self.query(sql)
