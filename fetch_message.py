"""
Fetch Message.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

Fetch Message class has the functionality to fetch message from sqs and published it in sns.
"""

from platformutils.sqs_utils import SQSUtils
from platformutils.sns_utils import SNSUtils

class FetchMessage(object):

    def __init__(self, r_name, level):
	self.sqs = SQSUtils(r_name, level)
	self.sns = SNSUtils(r_name, level)
	self.queue_name = 'd360-prod-mm-geo'
	self.arn = 'arn:aws:sns:us-east-1:951887592081:370-osm-dev'

    def byteify(self, input):
    	if isinstance(input, dict):
            return {self.byteify(key): self.byteify(value)
                for key, value in input.iteritems()}
    	elif isinstance(input, list):
            return [self.byteify(element) for element in input]
        elif isinstance(input, unicode):
            return input.encode('utf-8')
    	else:
            return input

    def read_message(self):
	"""
	This method is used to read message from sqs.
	Publish message to sns
	Delete the published message from sqs
        """
	response = self.sqs.read_queue_message(self.queue_name)
	data = self.byteify(response)
	try:
            msg = data['Messages']
	    for m in msg:
		self.sns.publish_message(self.arn, m['Body'])
		self.delete_message(m['ReceiptHandle'], self.queue_name)
		self.read_message()
    	except Exception as e:
            pass    
