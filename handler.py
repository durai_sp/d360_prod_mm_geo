"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

handle functionality is used to initiate d360_prod_mm_geo.
"""
from dashboard import Dashboard

def handler():
    d = Dashboard()
    d.dashboard()

handler()
